freezone-banshee-playlists.rb
=============================

freezone-banshee-playlists is a simple script to scrape the [iiNet freezone radio web-page](freezone.iinet.net.au/radio)
and insert the iiNet free radio stations into your [banshee](www.banshee.fm) 2.21 music player. This allows you to play
the iiNet radio streams for free directly in banshee. Keep in mind that the data used to listen to these radio streams is
not counted against your monthly quota. Of course if you are not an iiNet customer then this doesn't apply and the cost is
dependent on your ISP.

Pre-requisites
--------------

This script relies on ruby and the following gems:

* hpricot
* open-uri
* awesome\_print
* active\_record

I have only tested using:

* operating system - [Fedora 16](www.fedoraproject.org)
* ruby - ruby 1.9.3p362 (2012-12-25 revision 38607) [x86\_64-linux]
* hpricot - 0.8.6
* awesone\_print - 1.1.0
* active\_record - 3.2.11
* sqlite - 1.3.7
* sqlite-ruby - 1.3.3
* banshee - 2.2.1

See [ruby](www.ruby-lang.org) and [gems](www.rubygems.org) for installation instructions for your environment.

Running
-------

To run, open a terminal and change to the directory that you downloaded
the script to. To execute the freezone-banshee-playlists.rb ruby script type:

    cp ~/.config/banshee-1/banshee.db .
    chmod +x freezone-banshee-playlists.rb
    ./freezone-banshee-playlists.rb
    mv ~/.config/banshee-1/banshee.db ~/.config/banshee-1/banshee.db.$(date +%F)
    mv banshee.db ~/.config/banshee-1/banshee.db

Then restart banshee. The streams from the iiNet page should now appear under the Radio library.

The above may appear convoluted. This is mainly to ensure that you can recover from database corruption. This is
because the script directly manipulates the sqlite database that banshee uses. Following the above recipe means that
if banshee won't start afterwards you can simple do (assuming you do it on the same day):

    cp ~/.config/banshee-1/banshee.db.$(date +%F) ~/.config/banshee-1/banshee.db

and your original database is restored. This "should" now allow banshee to start without error.



