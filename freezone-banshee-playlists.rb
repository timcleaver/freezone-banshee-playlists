#!/usr/bin/env ruby

require 'rubygems'
require 'hpricot'
require 'open-uri'
require 'awesome_print'
require 'active_record'
require 'logger'
require 'digest/md5'
require 'date'

# we use the sqlite client to pull apart the banshee database
# ~/.config/banshee-1/banshee.db
#
class Station < ActiveRecord::Base
  self.table_name = 'CoreTracks'
  self.primary_key = 'TrackID'
  establish_connection(
    :adapter => 'sqlite3',
    :database => 'banshee.db'
  )
  belongs_to :source
end

class Source < ActiveRecord::Base
  self.table_name = 'CorePrimarySources'
  self.primary_key = 'PrimarySourceID'
  establish_connection(
    :adapter => 'sqlite3',
    :database => 'banshee.db'
  )
  has_many :stations, :class_name => "Station", :foreign_key => 'PrimarySourceID'
end

class Artist < ActiveRecord::Base
  self.table_name = 'CoreArtists'
  self.primary_key = 'ArtistID'
  establish_connection(
    :adapter => 'sqlite3',
    :database => 'banshee.db'
  )
end

class Album < ActiveRecord::Base
  self.table_name = 'CoreAlbums'
  self.primary_key = 'AlbumID'
  establish_connection(
    :adapter => 'sqlite3',
    :database => 'banshee.db'
  )
end

# enable to see active record db logging
#ActiveRecord::Base.logger = Logger.new(STDOUT)

# enable to see the existing stations
#puts "Existing Radio Stations: "
radio = Source.includes(:stations).where(:StringID => 'InternetRadioSource-internet-radio').first
#radio.stations.each { |station| ap station }

=begin
#<Station
# PrimarySourceID: 5,
# TrackID: 26185,
# ArtistID: 318,
# AlbumID: 1610,
# TagSetID: 0,
# ExternalID: 0,
# MusicBrainzID: nil,
# Uri: "http://media.on.net/radio/198.m3u",
# MimeType: nil,
# FileSize: 0,
# BitRate: 0,
# SampleRate: 0,
# BitsPerSample: 0,
# Attributes: 5,
# LastStreamError: 0,
# Title: ".977 The 90s Channel",
# TitleLowered: "977 the 90s channel",
# TitleSort: nil,
# TitleSortKey: "\a3\f\xB4\f\x90\f\x90\a\x02\x0E\x99\x0E,\x0E!\a\x02\f\xB4\f\x03\x0E\x91\a\x02\x0E\n\x0E,\x0E\x02\x0Ep\x0Ep\x0E!\x0EH\x01\x01\x01\x01\x00",
# TrackNumber: 0,
# TrackCount: 0,
# Disc: 0,
# DiscCount: 0,
# Duration: 0,
# Year: 0,
# Genre: "90s",
# Composer: nil,
# Conductor: nil,
# Grouping: nil,
# Copyright: nil,
# LicenseUri: nil,
# Comment: nil,
# Rating: 0,
# Score: 0,
# PlayCount: 0,
# SkipCount: 0,
# LastPlayedStamp: nil,
# LastSkippedStamp: nil,
# DateAddedStamp: 1357912376,
# DateUpdatedStamp: 1357912376,
# MetadataHash: "44c3ae96a22055642599f407c7e29529",
# BPM: 0,
# LastSyncedStamp: 1357912376,
# FileModifiedStamp: 0
# >
=end

unknown_artist = Artist.where(:NameLowered => 'unknown artist').first_or_create(:Name => 'Unknown Artist', :Rating => 0)
unknown_album = Album.where(:TitleLowered => 'unknown album').first_or_create(
  :ArtistID => unknown_artist.ArtistID,
  :Title => 'Unknown Album',
  :ReleaseDate => 0,
  :Duration => 0,
  :Year => 0,
  :ArtistName => unknown_artist.Name,
  :ArtistNameLowered => unknown_artist.NameLowered,
  :Rating => 0
)

doc = Hpricot open 'http://freezone.iinet.com.au/radio'
stations = (doc/'//div[@class="radio-items"]/article')
stations.each do |station|
  title = (station/'div[@class="radio-table-details"]/h4/a[@class="radio-title"]').inner_html
  uri = (station/'div[@class="radio-table-status"]/p/a').first.attributes['href']
  genre = (station/'div[@class="radio-table-column"]/h4').inner_html
  station = Station.where(:PrimarySourceID => radio.PrimarySourceID, :Title => title).first_or_create(:Genre => genre, :uri => uri)
  station.Uri = uri
  # zero the following fields
  station.TagSetID = station.ExternalID = station.FileSize = station.BitRate = station.SampleRate = 0
  station.BitsPerSample = station.TrackNumber = station.TrackCount = station.Disc = station.DiscCount = 0
  station.Duration = station.Year = station.Rating = station.Score = station.PlayCount = station.SkipCount = 0
  station.BPM = station.FileModifiedStamp = 0
  station.TitleLowered = station.Title.downcase
  stations.Genre = 'Unknown' if station.Genre.nil?
  station.MetadataHash = Digest::MD5.hexdigest [ '', '', station.Genre, station.Title, station.TrackNumber, station.Year ].join
  station.LastSyncedStamp = station.DateAddedStamp = station.DateUpdatedStamp = DateTime.now.to_i;
  # since I haven't worked out how mono generates sort keys from text,
  # we simply set them the lowercase version of the title (won't work
  # internationalized users, titles, etc)
  station.TitleSortKey = station.TitleLowered

  station.ArtistID = unknown_artist.ArtistID
  station.AlbumID = unknown_album.AlbumID

  station.save
end

# call through the sources to refresh the station list
#Source.includes(:stations).where(:StringID => 'InternetRadioSource-internet-radio').first.stations.each do |station|
#  puts "Uri: #{station.Uri}, Title: #{station.Title}, Genre: #{station.Genre}"
#end

